# File converter
This converter function can convert JSON, CSV, XML, XLSX, DB and SQL format into JSON, CSV, XML, XLSX, DB and SQL format.

| Format | CSV | JSON | XML | XLSX | SQL |
|:------:|:---:|:----:|:---:|:----:|:---:|
|  CSV   |     |  ✔   |  ✔  |  ✔   |  ✔  |
|  JSON  |  ✔  |      |  ✔  |  ✔   |  ✔  |
|  XML   |  ✔  |  ✔   |     |  ✔   |  ✔  |
|  XLSX  |  ✔  |  ✔   |  ✔  |      |  ✔  |
|  SQL   |  ✔  |  ✔   |  ✔  |  ✔   |     |


import pandas as pd
import xml.etree.ElementTree as ET
import sqlite3
import subprocess
import os

def file_converter(input_path, output_format):

    # separate file name and format
    input_format = input_path.split('.')[-1]
    
    # json to df
    if input_format == 'json':
        with open(input_path, 'r') as f:
            df = pd.read_json(f)

    # xml to df
    elif input_format == 'xml':
        tree = ET.parse(input_path)
        root = tree.getroot()
        data = []
        for row in root.findall('row'):
            item = {}
            for child in row:
                item[child.tag] = child.text
            data.append(item)
        df = pd.DataFrame(data)

    # csv to df
    elif input_format == 'csv':
        df = pd.read_csv(input_path)
    
    # excel to df
    elif input_format == 'xlsx':
        df = pd.read_excel(input_path)
    
    # sql to df
    elif input_format == 'db':
        conn = sqlite3.connect(input_path)
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table_name = cursor.fetchone()[0]
        df = pd.read_sql_query(f"SELECT * FROM {table_name}", conn)
        conn.close()

    elif input_format == 'sql':
        with open(input_path, 'r') as f:
            sql_script = f.read()
        
        conn = sqlite3.connect(':memory:')
        cursor = conn.cursor()
        
        cursor.executescript(sql_script)
        
        cursor.execute("SELECT * FROM sqlite_master WHERE type='table';")
        table_name = cursor.fetchone()[1]
        df = pd.read_sql_query(f"SELECT * FROM {table_name}", conn)
        
        conn.close()

    else:
        raise ValueError("Unsupported input file format")
    

    # split input path and use just file name with dot and output formate 
    output_path = os.path.splitext(input_path)[0] + '.' + output_format

    # create df to json file
    if output_format == 'json':
        df.to_json(output_path, orient='records')
    
    # create df to xml file
    elif output_format == 'xml':
        df.to_xml(output_path, root_name='root', row_name='row')
    
    # create df to csv file
    elif output_format == 'csv':
        df.to_csv(output_path, index=False)
    
    # create df to excel file
    elif output_format == 'xlsx':
        df.to_excel(output_path, index=False)
    
    # create df to sql file
    elif output_format == 'db' or output_format == 'sql':
        
        if output_format == 'sql':
            output_format = 'db' 
            output_path = os.path.splitext(output_path)[0] + '.db'

        conn = sqlite3.connect(output_path)
        df.to_sql('data', conn, if_exists='replace', index=False)
        conn.close()
        
        # Convert SQLite database to SQL dump file
        output_dump_path = os.path.splitext(output_path)[0] + '.sql'
        subprocess.run(["sqlite3", output_path, ".dump"], stdout=open(output_dump_path, 'w'))
       
    else:
        raise ValueError("Unsupported output format")

file_converter('file/data.csv', 'json')
